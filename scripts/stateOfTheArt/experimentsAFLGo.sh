#!/bin/bash --posix
# usage: ./experimentsAFLGo.sh 60 
# param: timeout (in minutes)

declare -a benchmarks=("../../src/stateOfTheArt/aflgo/arclength" \
  "../../src/stateOfTheArt/aflgo/rayCasting" \
  "../../src/stateOfTheArt/aflgo/linearSVC" \
  "../../src/stateOfTheArt/aflgo/nbody" \
  "../../src/stateOfTheArt/aflgo/linpack" \
  "../../src/stateOfTheArt/aflgo/fbenchV1" \
  "../../src/stateOfTheArt/aflgo/fbenchV2"\
  "../../src/stateOfTheArt/aflgo/lulesh"\
  "../../src/stateOfTheArt/aflgo/invertedPendulum")

TIMELIMIT=$((1 * 60))

# Runs AFLGo on the whole code
echo "******************** AFLGo Results ********************"
for file in "${benchmarks[@]}"
do
  echo ${file}
  cd ${file}
  ./aflgo.sh ${TIMELIMIT}
  cd ../
done
