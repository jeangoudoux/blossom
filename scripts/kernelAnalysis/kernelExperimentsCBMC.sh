#!/bin/bash --posix
# usage: ./kernelExperimentsCBMC.sh 10
# param: timeout (in minutes)

declare -a files=(
  "../../src/kernels/cbmc/Nbody.c" \
  "../../src/kernels/cbmc/Fbench.c")

TIMELIMIT=$((1 * 60))

#finds the cbmc executable
CBMC=$(find / ! -path '/proc' -type f -executable -name "cbmc")

# Runs CBMC on the kernel
echo "******************** CBMC Results ********************"
for file in "${files[@]}"
do
  echo ${file}
  timeout --preserve-status ${TIMELIMIT} ${CBMC} --LP64 --drop-unused-functions --nan-check --float-overflow-check --div-by-zero-check --trace --unwind 10 $file
done
