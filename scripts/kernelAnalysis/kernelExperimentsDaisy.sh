#!/bin/bash --posix

# How to install Daisy:
# apt-get install libmpfr4
# [install Z3]
# git clone git-rts@gitlab.mpi-sws.org:AVA/daisy.git daisy-source
# cd daisy-source 
# sbt compile
# sbt script
# cd ../
# cp daisy-source/daisy ./
# cp daisy-source/library/ ./
#
# Daisy can then be run from this directory (scripts) using './daisy' 

echo "Verifying arclength kernel:"
./daisy --silent ../../src/kernels/daisy/Arclength.scala 

echo "Optimizing arclength kernel:"
./daisy --silent --rewrite ../../src/kernels/daisy/Arclength.scala

echo "Verifying LinearSVC kernel:"
./daisy --silent ../../src/kernels/daisy/LinearSVC.scala 

echo "Optimizing LinearSVC kernel:"
./daisy --silent --rewrite ../../src/kernels/daisy/LinearSVC.scala

echo "Verifying Raycasting kernel:"
./daisy --silent ../../src/kernels/daisy/RayCasting.scala 

echo "Optimizing Raycasting kernel:"
./daisy --silent --rewrite ../../src/kernels/daisy/RayCasting.scala

echo "Verifying Nbody kernels:"
./daisy --silent ../../src/kernels/daisy/Nbody.scala 

echo "Optimizing Nbody kernels:"
./daisy --silent --rewrite ../../src/kernels/daisy/Nbody.scala

echo "Verifying Lulesh kernels:"
./daisy --silent ../kernels/daisy/Lulesh.scala

echo "Optimizing Lulesh kernels:"
./daisy --silent --rewrite ../../src/kernels/daisy/Lulesh.scala

echo "Verifying Linpack kernels:"
./daisy --silent ../../src/kernels/daisy/Linpack.scala

echo "Optimizing Linpack kernels:"
./daisy --silent --rewrite ../../src/kernels/daisy/Linpack.scala

echo "Verifying InvertedPendulum kernels:"
./daisy --silent ../../src/kernels/daisy/InvertedPendulum.scala 

echo "Optimizing InvertedPendulum kernels:"
./daisy --silent --rewrite ../../src/kernels/daisy/InvertedPendulum.scala

echo "Verifying FBenchV1 (without library function) kernels:"
./daisy --silent ../../src/kernels/daisy/FBenchV1.scala

echo "Optimizing FBenchV1 (without library function) kernels:"
./daisy --silent --rewrite ../../src/kernels/daisy/FBenchV1.scala

echo "Verifying FBenchV2 (with library function) kernels:"
./daisy --silent ../../src/kernels/daisy/FBenchV2.scala

echo "Optimizing FBenchV2 (with library function) kernels:"
./daisy --silent --rewrite ../../src/kernels/daisy/FBenchV2.scala
