#!/bin/bash --posix

# usage: ./bb_plotting.sh arclength 23 5 
# params: name of benchmark (without .c), random seed to use, time limit to use

if [ "$1" = "" ]; then
  echo "Please provide a benchmark name"
  exit 1
else
  bench_name="$1"
  file="../../src/BBrangePlot/${bench_name}.c"
fi
echo "file: ${file}"

if [ "$2" = "" ]; then
  echo "Please provide a seed"
  exit 1
else
  seed="$2"
fi
echo "seed: ${seed}"

if [ "$3" = "" ]; then
  echo "Please provide a time_limit"
  exit 1
else
  time_limit="$3"
fi
echo "time limit: ${time_limit}"

# remove previous executable so we are sure to run the right onw
rm plotting.out

if [ ${file} == "../../src/BBrangePlot/nbody.c" ]; then
  g++ $file -O3 -lm -o plotting.out 
elif [ ${file} == "../../src/BBrangePlot/lulesh.cc" ]; then
  g++ $file -O3 -lm -o plotting.out
else
  gcc $file -O3 -lm -o plotting.out
fi
echo $res
echo "finished compilation, going to run"

# generate result directory
if [ ! -d ../../results ]; then
  mkdir ../../results
  mkdir ../../results/blackbox
elif [ ! -d ../../results/blackbox ]; then
  mkdir ../../results/blackbox
fi

# clean the tmp directory
rm -rf ../../results/blackbox/plotting/
mkdir ../../results/blackbox/plotting/

# will generate files in results/blackbox/plotting/
./plotting.out $seed $time_limit

tmp_dir=../../results/blackbox/plotting/*
for f in $tmp_dir
do
  echo "Generating histograms for file: $f"
  python3 range_plotting.py $f 
done
