
import numpy as np
import matplotlib.pyplot as plt
import sys

fileName = sys.argv[1]

data = np.loadtxt(fileName)

def plotHistogram(d, index):
    plt.clf()
    f = plt.figure()
    n, bins, patches = plt.hist(x=d, bins='auto', color='#0504aa',
                            alpha=0.7, rwidth=0.85)
    plt.grid(axis='y', alpha=0.75)
    plt.yscale('log', nonposy='clip')
    plt.xlabel('Value')
    plt.ylabel('Frequency')
    plt.title(f'Histogram for variable #{index}')
    #maxfreq = n.max()
    # Set a clean upper y-axis limit.
    #plt.ylim(ymax=np.ceil(maxfreq / 10) * 10 if maxfreq % 10 else maxfreq + 10)
    #plt.show()
    f.savefig(fileName.replace('.txt', f'_plot_{index}.png'), bbox_inches='tight')
    #plt.show()


if data.ndim == 1: # 1 variable only
    plotHistogram(data, 1)

else:
    for i in range(len(data[0])):
        d = data[:, i]
        print
        minX = min(d)
        maxX = max(d)
        print(f'min: {minX}, max: {maxX}')
        plotHistogram(d, i + 1)
        
