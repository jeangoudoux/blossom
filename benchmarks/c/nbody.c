/*
  Code taken from Rosetta Code (https://rosettacode.org/wiki/N-body_problem#C).
  Simulates the interaction of several masses under gravity. 
  Takes as input a configuration file specifying the number of masses and their
  positions, in the following format:
  <Gravitational Constant> <Number of bodies(N)> <Time step>
  <Mass of M1>
  <Position of M1 in x,y,z co-ordinates>
  <Initial velocity of M1 in x,,y,z components>
  ...
  <And so on for N bodies>
*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

typedef struct{
  double x,y,z;
}vector;

#define bodies 3

double masses[3];
double GravConstant = 0.01; /* Test with working input ranges */
int timeSteps = 10;

vector *positions,*velocities,*accelerations;

masses = (double*)malloc(bodies*sizeof(double));
positions = (vector*)malloc(bodies*sizeof(vector));
velocities = (vector*)malloc(bodies*sizeof(vector));
accelerations = (vector*)malloc(bodies*sizeof(vector));

vector addVectors(vector a,vector b){
  vector c = {a.x+b.x,a.y+b.y,a.z+b.z};

  return c;
}

vector scaleVector(double b,vector a){
  vector c = {b*a.x,b*a.y,b*a.z};

  return c;
}

vector subtractVectors(vector a,vector b){
  vector c = {a.x-b.x,a.y-b.y,a.z-b.z};

  return c;
}

double mod(vector a){
  return sqrt(a.x*a.x + a.y*a.y + a.z*a.z);
}

void initiateSystem(char* fileName){
  int i;
  FILE* fp = fopen(fileName,"r");

  fscanf(fp,"%lf%d%d",&GravConstant,&bodies,&timeSteps);

  masses = (double*)malloc(bodies*sizeof(double));
  positions = (vector*)malloc(bodies*sizeof(vector));
  velocities = (vector*)malloc(bodies*sizeof(vector));
  accelerations = (vector*)malloc(bodies*sizeof(vector));

  for(i=0;i<bodies;i++){
    fscanf(fp,"%lf",&masses[i]);
    fscanf(fp,"%lf%lf%lf",&positions[i].x,&positions[i].y,&positions[i].z);
    fscanf(fp,"%lf%lf%lf",&velocities[i].x,&velocities[i].y,&velocities[i].z);
  }

  fclose(fp);
}

void resolveCollisions(){
  int i,j;

  for(i=0;i<bodies-1;i++)
    for(j=i+1;j<bodies;j++){
      if(positions[i].x==positions[j].x && positions[i].y==positions[j].y && positions[i].z==positions[j].z){
        vector temp = velocities[i];
        velocities[i] = velocities[j];
        velocities[j] = temp;
      }
    }
}

/* Kernel 1 */
vector numerical_kernel1(double mass, vector position_i, vector position_j, vector acceleration) {
  vector acceleration_computed = addVectors(acceleration,scaleVector(GravConstant*mass/
  pow(mod(subtractVectors(position_i,position_j)),3),subtractVectors(position_j,position_i)));
  return acceleration_computed;
}

void computeAccelerations(){
  int i,j;
  for(i=0;i<bodies;i++){
    accelerations[i].x = 0;
    accelerations[i].y = 0;
    accelerations[i].z = 0;
    for(j=0;j<bodies;j++){
      if(i!=j){
        accelerations[i] = numerical_kernel(masses[j], positions[i], positions[j], accelerations[i]);
      }
    }
  }
}

void computePositions(){
  int i;

  for(i=0;i<bodies;i++)
    positions[i] = addVectors(positions[i],addVectors(velocities[i],scaleVector(0.5,accelerations[i])));
}

/* computeVelocities() function */
void numerical_kernel2(){
  int i;

  for(i=0;i<bodies;i++)
    velocities[i] = addVectors(velocities[i],accelerations[i]);
}

void simulate(){
  computeAccelerations();
  computePositions();
  numerical_kernel2(); 
  resolveCollisions();
}

int main(int argC,char* argV[])
{
  /* Range 1 of input variables: for all i < #bodies (error_input_range)
        masses[i]: [5.0, 50.0]
        positions[i].x: [0.0, 10.0]
        positions[i].y: [0.0, 10.0]
        positions[i].z: [0.0, 10.0]
        velocities[i].x: [10.0, 50.0]
        velocities[i].y: [10.0, 50.0]
        velocities[i].z: [10.0, 50.0]
  */
  
  /* RANGES 2:
     masses[0] in [0.9, 1.1]
     positions[0].x in [0.0, 0.5]
     positions[0].y in [0.0, 0.5]
     positions[0].z in [0.0, 0.5]
     velocities[0].x in [0.001,0.019]
     velocities[0].y in [0.0, 0.5]
     velocities[0].z in [0.0, 0.5]
     masses[1] in [0.001, 0.9]
     positions[1].x in [0.9, 1.1]
     positions[1].y in [0.9, 1.1]
     positions[1].z in [0.0, 0.5]
     velocities[1].x in [0.0, 0.5]
     velocities[1].y in [0.0, 0.5]
     velocities[1].z in [0.01, 0.03]
     masses[2] in [0.0001, 0.0019]
     positions[2].x in [0.0, 0.5]
     positions[2].y in [0.9, 1.1]
     positions[2].z in [0.9, 1.1]
     velocities[2].x in [0.001, 0.019]
     velocities[2].y in [-0.019, -0.001]
     velocities[2].z in [-0.019, -0.001]
  */
  
    int i,j;

  if(argC!=2)
    printf("Usage : %s <file name containing system configuration data>",argV[0]);
  else{

    initiateSystem(argV[1]);

    printf("Body   :     x              y               z           |           vx              vy              vz   ");
    for(i=0;i<timeSteps;i++){
      printf("\nCycle %d\n",i+1);
      simulate();
      for(j=0;j<bodies;j++)
	printf("Body %d : %lf\t%f\t%lf\t|\t%lf\t%lf\t%lf\n",j+1,positions[j].x,positions[j].y,positions[j].z,velocities[j].x,velocities[j].y,velocities[j].z);
    }
  } 
  return 0;
}
