#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <stdbool.h> 

double coefficients[3][4] = {{0.1842392154564795, 0.451227272556133, -0.8079415307840663, -0.45071538868087374}, {0.05674216334215214, -0.8981194157558171, 0.4083958984619029, -0.9606849222530257}, {-0.8507702027651778, -0.9868272809923471, 1.3808941178761502, 1.8652996296254423}};
double intercepts[3] = {0.10956209204126635, 1.677892675644221, -1.7095873485238913};

/* File to print the kernel inputs */
FILE *fptr;

/* Generating random doubles */
double get_random(double lowerBound, double upperBound){
  double f = (double)rand() / RAND_MAX;
  return lowerBound + f * (upperBound - lowerBound);
}


double numerical_kernel(double features[], int i) {
/* Keeping track of the input points */
  fprintf(fptr,"%.20e %.20e %.20e %.20e\n", features[0], features[1], features[2], features[3]);
/* =========================================================== */
  double prob = 0.0;
  for (int j = 0, jl = sizeof(coefficients[0]) / sizeof (coefficients[0][0]); j < jl; j++) {
    prob += coefficients[i][j] * features[j];
  }
  return prob;
}


int predict (double features[]) {
  double class_val = -INFINITY;
  int class_idx = -1;
  int i, il, j, jl;
  for (i = 0, il = sizeof(coefficients) / sizeof (coefficients[0]); i < il; i++) {    
    double prob = numerical_kernel(features, i);
    if (prob + intercepts[i] > class_val) {
      class_val = prob + intercepts[i];
      class_idx = i;
    }
  }
  return class_idx;
}

int main(int argc, char *argv[]) {
  /* getting the seed and time as command line arguments */
  if(argc <= 2) {
    printf("Expects 2 arguments!\n");
    exit(1);
  }
  int seed = atoi(argv[1]);
  int maxTime = atoi(argv[2]);
  
  char* base = "../../results/blackbox/plotting/linearSVC_"; 
  char* extension = ".txt";
  char fileSpec[strlen(base)+strlen(argv[1])+strlen(extension)+1];
  snprintf( fileSpec, sizeof( fileSpec ), "%s%s%s", base, argv[1], extension );
  fptr = fopen(fileSpec,"w"); 
  double features[4]; 
  double time_spent;
  clock_t begin = clock();
/* =============================== */
  srand(seed);
  do {
/* Generating ranges randomly */
  	features[0] = get_random(4.0, 8.0);
    features[1] = get_random(2.0, 4.5);
    features[2] = get_random(1.0, 7.0);
    features[3] = get_random(0.1, 2.5);
/* =========================================*/

    predict(features);

    /* Stops the execution after specified time */
    clock_t end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  } while (time_spent <= maxTime);

  fclose(fptr);
/* ========================================================= */
  return 0;
}
