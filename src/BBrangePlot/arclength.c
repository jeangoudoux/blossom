#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h> 

/* File to print the kernel inputs */
FILE *fptr;

/* Generating random intergers */
int get_random(int min, int max){
   return (min + (rand() % (max - min)));
}

long double numerical_kernel(long double x) {
/* Keeping track of the input points */
  fprintf(fptr,"%.20e\n", x);
/* =============================================================*/     
  int k, n = 5;
  long double t1;
  long double d1 = 1.0L;
  t1 = x;
  for( k = 1; k <= n; k++ ) {
    d1 = 2.0 * d1;
    t1 = t1 + sin (d1 * x) / d1;
  }
  return t1;
}


int main(int argc, char *argv[]) {
  /* getting the seed and time as command line arguments */
  if(argc <= 2) {
    printf("Expects 2 arguments!\n");
    exit(1);
  }
  int seed = atoi(argv[1]);
  int maxTime = atoi(argv[2]);
  char* base = "../../results/blackbox/plotting/arclength_"; 
  char* extension = ".txt";
  char fileSpec[strlen(base)+strlen(argv[1])+strlen(extension)+1];
  snprintf( fileSpec, sizeof( fileSpec ), "%s%s%s", base, argv[1], extension );
  fptr = fopen(fileSpec,"w");
  double time_spent;
  clock_t time_begin = clock();
  srand(seed);
  /* ==================================================== */
  int n, i;
  do {
    n = get_random(1, 1000000); // generating program inputs randomly
    long double h, t2, dppi;
    long double s1;
    long double t1 = -1.0;

    dppi = acos(t1);
    s1 = 0.0;
    t1 = 0.0;
    h = dppi / n;
    for( i = 1; i <= n; i++ ) {
      t2 = numerical_kernel(i * h);
      s1 = s1 + sqrt (h*h + (t2 - t1)*(t2 - t1));
      t1 = t2;
    }

/* Stops the execution after specified time */
    clock_t time_end = clock();
    time_spent = ((double)(time_end - time_begin)) / CLOCKS_PER_SEC;
  } while(time_spent <= maxTime);
  fclose(fptr);
  return 0;
}
