/* Code taken from Rosetta Code (https://rosettacode.org/wiki/N-body_problem#C).
  Simulates the interaction of several masses under gravity. 
  Takes as input a configuration file specifying the number of masses and their
  positions, in the following format:
  <Gravitational Constant> <Number of bodies(N)> <Time step>
  <Mass of M1>
  <Position of M1 in x,y,z co-ordinates>
  <Initial velocity of M1 in x,,y,z components>
  ...
  <And so on for N bodies>
*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <stdbool.h> 

typedef struct{
  double x,y,z;
} vector;

#define bodies 3
double GravConstant = 0.01;
int timeSteps = 10;
double masses[3];
vector *positions = (vector*)malloc(bodies*sizeof(vector));
vector *velocities = (vector*)malloc(bodies*sizeof(vector));
vector *accelerations = (vector*)malloc(bodies*sizeof(vector));

/* global variables to store the ranges of inputs */
vector velocities_max[bodies];
vector velocities_min[bodies];
vector accelerations_max_ker1[bodies];
vector accelerations_min_ker1[bodies];

vector position_i_max;
vector position_i_min;
vector position_j_max;
vector position_j_min;
vector accelerations_max_ker2;
vector accelerations_min_ker2;

bool initial_ker1 = true;
bool initial_ker2 = true;
FILE *fptr1, *fptr2;

/* Generating random doubles */
double get_random(double lowerBound, double upperBound) {
  double f = (double)rand() / RAND_MAX;
  return lowerBound + f * (upperBound - lowerBound);
}

vector addVectors(vector a,vector b) {
  vector c = {a.x+b.x,a.y+b.y,a.z+b.z};

  return c;
}

vector scaleVector(double b,vector a) {
  vector c = {b*a.x,b*a.y,b*a.z};

  return c;
}

vector subtractVectors(vector a,vector b) {
  vector c = {a.x-b.x,a.y-b.y,a.z-b.z};

  return c;
}

double mod(vector a) {
  return sqrt(a.x*a.x + a.y*a.y + a.z*a.z);
}

/* Kernel Function */
void numerical_kernel2() {
/* Generating the ranges of the inputs to the kernel function */
  if(initial_ker1){
    for(int i=0;i<bodies;i++) {
      velocities_min[i].x = velocities[i].x;
      velocities_min[i].y = velocities[i].y;
      velocities_min[i].z = velocities[i].z;
      velocities_max[i].x = velocities[i].x;
      velocities_max[i].y = velocities[i].y;
      velocities_max[i].z = velocities[i].z;
      accelerations_max_ker1[i].x = accelerations[i].x;
      accelerations_max_ker1[i].y = accelerations[i].y;
      accelerations_max_ker1[i].z = accelerations[i].z;
      accelerations_min_ker1[i].x = accelerations[i].x;
      accelerations_min_ker1[i].y = accelerations[i].y;
      accelerations_min_ker1[i].z = accelerations[i].z;
      initial_ker1 = false;
    }
  } else {
    for(int i=0;i<bodies;i++) {
      if (velocities[i].x < velocities_min[i].x) {
        velocities_min[i].x = velocities[i].x;
      }
      if (velocities[i].y < velocities_min[i].y) {
        velocities_min[i].y = velocities[i].y;
      }
      if (velocities[i].z < velocities_min[i].z) {
        velocities_min[i].z = velocities[i].z;
      }
      if (velocities[i].x > velocities_max[i].x) {
        velocities_max[i].x = velocities[i].x;
      }
      if (velocities[i].y > velocities_max[i].y) {
        velocities_max[i].y = velocities[i].y;
      }
      if (velocities[i].z > velocities_max[i].z) {
        velocities_max[i].z = velocities[i].z;
      }
      if (accelerations[i].x < accelerations_min_ker1[i].x) {
        accelerations_min_ker1[i].x = accelerations[i].x;
      }
      if (accelerations[i].y < accelerations_min_ker1[i].y) {
        accelerations_min_ker1[i].y = accelerations[i].y;
      }
      if (accelerations[i].z < accelerations_min_ker1[i].z) {
        accelerations_min_ker1[i].z = accelerations[i].z;
      }
      if (accelerations[i].x > accelerations_max_ker1[i].x) {
        accelerations_max_ker1[i].x = accelerations[i].x;
      }
      if (accelerations[i].y > accelerations_max_ker1[i].y) {
        accelerations_max_ker1[i].y = accelerations[i].y;
      }
      if (accelerations[i].z > accelerations_max_ker1[i].z) {
        accelerations_max_ker1[i].z = accelerations[i].z;
      }
    }
  }
/* =========================================================== */
  for(int i=0;i<bodies;i++) {
    velocities[i] = addVectors(velocities[i],accelerations[i]); 
  }
}

vector numerical_kernel1(double mass, vector position_i, vector position_j, vector acceleration) {
/* Generating the ranges of the inputs to the kernel function */ 
  if(initial_ker2) {
    position_i_min = position_i;
    position_i_max = position_i;
    position_j_min = position_j;
    position_j_max = position_j;
    accelerations_min_ker2 = acceleration;
    accelerations_max_ker2 = acceleration;
    initial_ker2 = false;
  } else {
      if (position_i.x < position_i_min.x) {
        position_i_min.x = position_i.x;
      }
      if (position_i.y < position_i_min.y) {
        position_i_min.y = position_i.y;
      }
      if (position_i.z < position_i_min.z) {
        position_i_min.z = position_i.z;
      }
      if (position_j.x < position_j_min.x) {
        position_j_min.x = position_j.x;
      }
      if (position_j.y < position_j_min.y) {
        position_j_min.y = position_j.y;
      }
      if (position_j.z < position_j_min.z) {
        position_j_min.z = position_j.z;
      }
      if (position_i.x > position_i_max.x) {
        position_i_max.x = position_i.x;
      }
      if (position_i.y > position_i_max.y) {
        position_i_max.y = position_i.y;
      }
      if (position_i.z > position_i_max.z) {
        position_i_max.z = position_i.z;
      }
      if (position_j.x > position_j_max.x) {
        position_j_max.x = position_j.x;
      }
      if (position_j.y > position_j_max.y) {
        position_j_max.y = position_j.y;
      }
      if (position_j.z > position_j_max.z) {
        position_j_max.z = position_j.z;
      }
      if (acceleration.x < accelerations_min_ker2.x) {
        accelerations_min_ker2.x = acceleration.x;
      }
      if (acceleration.y < accelerations_min_ker2.y) {
        accelerations_min_ker2.y = acceleration.y;
      }
      if (acceleration.z < accelerations_min_ker2.z) {
        accelerations_min_ker2.z = acceleration.z;
      }
      if (acceleration.x > accelerations_max_ker2.x) {
        accelerations_max_ker2.x = acceleration.x;
      }
      if (acceleration.y > accelerations_max_ker2.y) {
        accelerations_max_ker2.y = acceleration.y;
      }
      if (acceleration.z > accelerations_max_ker2.z) {
        accelerations_max_ker2.z = acceleration.z;
      }
    }
/* =========================================================== */
  vector acceleration_computed = addVectors(acceleration,scaleVector(GravConstant*mass/
  pow(mod(subtractVectors(position_i,position_j)),3),subtractVectors(position_j,position_i)));
  return acceleration_computed;
}

void resolveCollisions() {
  int i,j;
  for(i=0;i<bodies-1;i++) {
    for(j=i+1;j<bodies;j++){
      if(positions[i].x==positions[j].x && positions[i].y==positions[j].y && positions[i].z==positions[j].z){
        vector temp = velocities[i];
        velocities[i] = velocities[j];
        velocities[j] = temp;
      }
    }
  }
}

void computeAccelerations() {
  int i,j;
  for(i=0;i<bodies;i++){
    accelerations[i].x = 0;
    accelerations[i].y = 0;
    accelerations[i].z = 0;
    for(j=0;j<bodies;j++){
      if(i!=j){
        accelerations[i] = numerical_kernel1(masses[j], positions[i], positions[j], accelerations[i]);
      }
    }
  }
}

void computePositions() {
  int i;

  for(i=0;i<bodies;i++)
    positions[i] = addVectors(positions[i],addVectors(velocities[i],scaleVector(0.5,accelerations[i])));
}

void simulate() {
  computeAccelerations();
  computePositions();
  numerical_kernel2();
  resolveCollisions();
}

int main(int argc,char* argv[]) {
  /* getting the seed and time as command line arguments */
  if(argc <= 2) {
    printf("Expects 2 arguments!\n");
    exit(1);
  }
  int seed = atoi(argv[1]);
  int maxTime = 60 * atoi(argv[2]);
  char filename[128];
  double time_spent;
  clock_t time_begin = clock();
  srand(seed);
/* =============================== */
  do {
/* Generating ranges randomly */
    masses[0] = get_random(0.9, 1.1);
    positions[0].x = get_random(0.0, 0.5);
    positions[0].y = get_random(0.0, 0.5);
    positions[0].z = get_random (0.0, 0.5);
    velocities[0].x = get_random (0.001, 0.019);
    velocities[0].y = get_random (0.0, 0.5);
    velocities[0].z = get_random (0.0, 0.5);
    masses[1] = get_random (0.001, 0.9);
    positions[1].x = get_random (0.9, 1.1);
    positions[1].y = get_random (0.9, 1.1);
    positions[1].z = get_random (0.0, 0.5);
    velocities[1].x = get_random (0.0, 0.5);
    velocities[1].y = get_random (0.0, 0.5);
    velocities[1].z = get_random (0.01, 0.03);
    masses[2] = get_random (0.0001, 0.0019);
    positions[2].x = get_random (0.0, 0.5);
    positions[2].y = get_random (0.9, 1.1);
    positions[2].z = get_random (0.9, 1.1);
    velocities[2].x = get_random (0.001, 0.019);
    velocities[2].y = get_random (-0.019, -0.001);
    velocities[2].z = get_random (-0.019, -0.001);
/* =========================================*/
    for (int k = 0; k < timeSteps; k++) {
      simulate();
	  }
/* Stops the execution after specified mins and prints the ranges in the file */
    clock_t time_end = clock();
    time_spent = ((double)(time_end - time_begin)) / CLOCKS_PER_SEC;
  } while (time_spent <= maxTime); 
  fptr1 = fopen("kernel1_range.dat", "w");
  fptr2 = fopen("kernel2_range.dat", "w");

  fprintf(fptr1,"%.20e %.20e\n", position_i_min.x, position_i_max.x);
  fprintf(fptr1,"%.20e %.20e\n", position_i_min.y, position_i_max.y);
  fprintf(fptr1,"%.20e %.20e\n", position_i_min.z, position_i_max.z);
  fprintf(fptr1,"%.20e %.20e\n", position_j_min.x, position_j_max.x);
  fprintf(fptr1,"%.20e %.20e\n", position_j_min.y, position_j_max.y);
  fprintf(fptr1,"%.20e %.20e\n", position_j_min.z, position_j_max.z);
  fprintf(fptr1,"%.20e %.20e\n", accelerations_min_ker2.x, accelerations_max_ker2.x);
  fprintf(fptr1,"%.20e %.20e\n", accelerations_min_ker2.y, accelerations_max_ker2.y);
  fprintf(fptr1,"%.20e %.20e\n", accelerations_min_ker2.z, accelerations_max_ker2.z);

  for (int i = 0; i <bodies; i++) {
    fprintf(fptr2,"%.20e %.20e\n", velocities_min[i].x, velocities_max[i].x);
    fprintf(fptr2,"%.20e %.20e\n", velocities_min[i].y, velocities_max[i].y);
    fprintf(fptr2,"%.20e %.20e\n", velocities_min[i].z, velocities_max[i].z);
    fprintf(fptr2,"%.20e %.20e\n", accelerations_min_ker1[i].x, accelerations_max_ker1[i].x);
    fprintf(fptr2,"%.20e %.20e\n", accelerations_min_ker1[i].y, accelerations_max_ker1[i].y);
    fprintf(fptr2,"%.20e %.20e\n", accelerations_min_ker1[i].z, accelerations_max_ker1[i].z);
  }
  fclose(fptr1);
  fclose(fptr2);

/* ========================================================= */
  return 0;
}

