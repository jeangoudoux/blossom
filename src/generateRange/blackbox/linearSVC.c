#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <stdbool.h> 

double coefficients[3][4] = {{0.1842392154564795, 0.451227272556133, -0.8079415307840663, -0.45071538868087374}, {0.05674216334215214, -0.8981194157558171, 0.4083958984619029, -0.9606849222530257}, {-0.8507702027651778, -0.9868272809923471, 1.3808941178761502, 1.8652996296254423}};
double intercepts[3] = {0.10956209204126635, 1.677892675644221, -1.7095873485238913};

/* global variables to store the ranges of inputs */
double features_min[4];
double features_max[4];
bool initial = true;
FILE *fptr;

/* Generating random doubles */
double get_random(double lowerBound, double upperBound){
    double f = (double)rand() / RAND_MAX;
    return lowerBound + f * (upperBound - lowerBound);
}

double numerical_kernel(double features[], int i) {
/* Generating the ranges of the inputs to the kernel function */
  if(initial) {
    memcpy(features_min, features, 4*sizeof(double));
    memcpy(features_max, features, 4*sizeof(double));
    initial = false;
  } else {
    for (i = 0; i < 4; i++) {
      if (features_min[i] > features[i]) {
        features_min[i] = features[i];
      }
      if (features_max[i] < features[i]) {
        features_max[i] = features[i];
      }
    }
  } 
/* =========================================================== */
  double prob = 0.0;
  for (int j = 0, jl = sizeof(coefficients[0]) / sizeof (coefficients[0][0]); j < jl; j++) {
    prob += coefficients[i][j] * features[j];
  }
  return prob;
}


int predict (double features[]) {
  double class_val = -INFINITY;
  int class_idx = -1;
  int i, il, j, jl;
  for (i = 0, il = sizeof(coefficients) / sizeof (coefficients[0]); i < il; i++) {    
    double prob = numerical_kernel(features, i);
    if (prob + intercepts[i] > class_val) {
      class_val = prob + intercepts[i];
      class_idx = i;
    }
  }
  return class_idx;
}

int main(int argc, char *argv[]) {
  /* getting the seed and time as command line arguments */
  if(argc <= 2) {
    printf("Expects 2 arguments!\n");
    exit(1);
  }
  int seed = atoi(argv[1]);
  int maxTime = 60 * atoi(argv[2]);
  double time_spent;
  clock_t time_begin = clock();
  srand(seed);
  /* ==================================================== */
  double features[4];
  do {
    /* Generating ranges randomly */
  	features[0] = get_random(4.0, 8.0);
    features[1] = get_random(2.0, 4.5);
    features[2] = get_random(1.0, 7.0);
    features[3] = get_random(0.1, 2.5);
/* =========================================*/

    predict(features);

/* Stops the execution after specified mins and prints the ranges in the file */
    clock_t time_end = clock();
    time_spent = ((double)(time_end - time_begin)) / CLOCKS_PER_SEC;
  } while (time_spent <= maxTime);
  fptr = fopen("kernel_range.dat", "w");
  for (int i = 0; i<4; i++) {
    fprintf(fptr,"%.20e %.20e\n", features_min[i], 
      features_max[i]);
  }
  fclose(fptr);
/* ========================================================= */
  return 0;
}
