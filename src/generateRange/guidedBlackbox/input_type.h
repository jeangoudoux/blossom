#ifndef __INPUT_TYPE_H__
#define __INPUT_TYPE_H__

#include <stdlib.h>


typedef struct {
  int n;
} __in_t;

void copyIn(__in_t*, __in_t);

#endif /* __INPUT_TYPE_H__ */
