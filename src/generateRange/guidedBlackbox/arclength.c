#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include "queue.h"
#include "input_type.h"

/* global variables to store the ranges of inputs */
double input, input_min, input_max;
bool initial = true;
FILE *fptr;
bool new_range = false;

/* Generating random intergers */
int get_random(int min, int max){
   return (min + (rand() % (max - min)));
}

double numerical_kernel(double x) {
/* Generating the ranges of the inputs to the kernel function */
  if (initial) {
    input_min = input;
    input_max = input;
    initial = false;
    new_range = true;
  } else {
    if (input_min > input) {
      input_min = input;
      new_range = true;
    }
    if (input_max < input) {
      input_max = input;
      new_range = true;
    }
  }
/* =============================================================*/   
  int k, n = 5;
  double t1;
  double d1 = 1.0L;
  t1 = x;
  for( k = 1; k <= n; k++ ) {
    d1 = 2.0 * d1;
    t1 = t1 + sin (d1 * x) / d1;
  }
  return t1;
}


int main(int argc, char *argv[]) {
  /* getting the seed and time as command line arguments */
  if(argc <= 2) {
    printf("Expects 2 arguments!\n");
    exit(1);
  }
  int seed = atoi(argv[1]);
  int maxTime = 60 * atoi(argv[2]);
  char filename[128];
  double time_spent;
  clock_t time_begin = clock();
  srand(seed);
  /* ==================================================== */
  int n, i, t = 0, fuzz = 10;
  __queue_t *q = queueCreate();
  __in_t new_value;
  do {
    if (isEmpty(q)) {
      n = get_random(1, 1000000); // generating program inputs randomly
    }  else {
      n = q->head->in.n; // the input is the head of the queue
      queueDel(q); // remove the head of the queue
    }
    double h, t2, dppi;
    double s1;
    double t1 = -1.0;
    dppi = acos(t1);
    s1 = 0.0;
    t1 = 0.0;
    h = dppi / n;
    for( i = 1; i <= n; i++ ) {
      input = i * h;
      t2 = numerical_kernel(input);
      s1 = s1 + sqrt (h*h + (t2 - t1)*(t2 - t1));
      t1 = t2;
    }

    /* Fuzz input to create new inputs for the next turn */
    if (new_range == true) {
      new_value.n = n + fuzz; // Tentative, we need to adapt the fuzzing depending on the input type and input range 
      if (new_value.n > 1000000) { // Check if new value is in range (only upper bound if the fuzzing increases the value)
	new_value.n = 1000000;
      }
      queueAdd(q,new_value);
      new_value.n = n - fuzz;
      if (new_value.n < 1) { // Check if new value is in range (only lower bound if the fuzzing decreases the value)
	new_value.n = 1;
      }
      queueAdd(q,new_value);
      new_range = false;
    } 

    /* Stops the execution after specified mins and prints the ranges in the file */
    clock_t time_end = clock();
    time_spent = ((double)(time_end - time_begin)) / CLOCKS_PER_SEC;
  } while(time_spent <= maxTime);
  fptr = fopen("kernel_range.dat", "w");
  fprintf(fptr,"%.20e %.20e\n",input_min, input_max);
  fclose(fptr);
  queueDestroy(q);
  /* ========================================================= */

  return 0;

}
