#include "input_type.h"

void copyIn(__in_t* in, __in_t i) {

  __in_t *copy = (__in_t*) malloc (sizeof(__in_t));
  copy->n = i.n;

  *in = *copy;  
}
