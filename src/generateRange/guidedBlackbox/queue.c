#include "queue.h"

/* Create a new empty queue */
__queue_t *queueCreate(void) {
  __queue_t *q;
  q = malloc(sizeof(__queue_t));
  
  q->head = q->tail = 0;
  return q;
}

/* Test if the queue is empty */
int isEmpty(__queue_t *q) {
  return (q->head == 0);
}

/* Add a new element to the back of the queue */
void queueAdd(__queue_t *q, __in_t i) {
  __input_t *in = (__input_t*) malloc(sizeof(__input_t));
  copyIn(&(in->in),i);
  in->next = 0;

  if(q->head == 0) {
    q->head = in;
  } else if (q->tail == 0) {
    q->head->next = in;
    q->tail = in;
  } else {
    q->tail->next = in;
    q->tail=in;
  }
}

/* Remove used element at the head of the queue, return the new head */
void queueDel(__queue_t *q) {
  if (! isEmpty(q)) {
    q->head = q->head->next;
  }
}

/* Free a queue and all of its elements */
void queueDestroy(__queue_t *q) {
    while(!isEmpty(q)) {
        queueDel(q);
    }

    free(q);
}
