#ifndef __QUEUE_H__
#define __QUEUE_H__

#include <stdlib.h>
#include "input_type.h"

/* Queue elements */ 
typedef struct elt {
  struct elt *next;
  __in_t in;
} __input_t;

/* Queue structure saving candidate input for fuzzing */
typedef struct {
  __input_t *head;
  __input_t *tail;
} __queue_t;

__queue_t* queueCreate();
int isEmpty(__queue_t*);
void queueAdd(__queue_t*, __in_t);
void queueDel(__queue_t*);
void queueDestroy(__queue_t*);

#endif /* __QUEUE_H__ */
