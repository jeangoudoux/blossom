#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <assert.h>

#define pic 3.1415926535897932
#define cot(x) (1.0 / tan(x))

void numerical_kernel1(double iang_sin, double from_index, double to_index,
  double slope_angle, double obj_dist, double ker2_ray_height) {
  double rang_sin, axis_slope_angle, ray_height, object_distance, radius_of_curvature;
__CPROVER_assume(radius_of_curvature >= -78.1  && radius_of_curvature <= 27.05);
  rang_sin = (from_index / to_index) * (iang_sin);
  __CPROVER_assert(!(isinf(rang_sin)), "infinity");

  axis_slope_angle = slope_angle + iang_sin - rang_sin;
  __CPROVER_assert(!(isinf(axis_slope_angle)), "infinity");

  if (obj_dist != 0.0) {
    ray_height = obj_dist * slope_angle;
  __CPROVER_assert(!(isinf(ray_height)), "infinity");

  } else {
    ray_height = ker2_ray_height;
  }
  object_distance = ray_height / axis_slope_angle;
  __CPROVER_assert(!(isinf(object_distance)), "infinity");

}

void numerical_kernel2(double iang_sin, double from_index, double to_index,
  double slope_angle, double rad_curvature) {

  double iang, rang_sin, axis_slope_angle, sagitta, object_distance;
  double radius_of_curvature;
  __CPROVER_assume(radius_of_curvature >= -78.1 && radius_of_curvature <= 27.05);
  iang = asin(iang_sin);
  rang_sin = (from_index / to_index) * (iang_sin);
  __CPROVER_assert(!(isinf(rang_sin)), "infinity");

  axis_slope_angle = slope_angle + iang - asin(rang_sin);
  __CPROVER_assert(!(isinf(axis_slope_angle)), "infinity");

  sagitta = sin((slope_angle + iang) / 2.0);

  sagitta = 2.0 * radius_of_curvature * sagitta * sagitta;
  __CPROVER_assert(!(isinf(sagitta)), "infinity");

  object_distance = ((rad_curvature * sin(slope_angle + iang)) *
              cot(axis_slope_angle)) + sagitta;
  __CPROVER_assert(!(isinf(object_distance)), "infinity");
}

int main(void) {
  double ker1_iang_sin, ker1_from_index, ker1_to_index,
    ker1_slope_angle, ker1_obj_dist, ker1_ray_height; //inputs to kernel1

  double ker2_iang_sin, ker2_from_index, ker2_to_index,
    ker2_slope_angle, ker2_rad_curvature; //inputs to kernel2

  __CPROVER_assume(ker1_iang_sin >= -0.217475732986908 && ker1_iang_sin <=0.0739371534195933);
  __CPROVER_assume(ker1_from_index >= 1.0 && ker1_from_index <= 1.6164);
  __CPROVER_assume(ker1_to_index >= 1.0 && ker1_to_index <= 1.6164);
  __CPROVER_assume(ker1_slope_angle >= 0.0 && ker1_slope_angle <= 0.0991744034677826);
  __CPROVER_assume(ker1_obj_dist >= 0.0 && ker1_obj_dist <= 121.112061135518);
  __CPROVER_assume(ker1_ray_height >= 1.97 && ker1_ray_height <= 2.0);
  numerical_kernel1(ker1_iang_sin, ker1_from_index, ker1_to_index,
    ker1_slope_angle, ker1_obj_dist, ker1_ray_height);

  __CPROVER_assume(ker2_iang_sin >= -0.218662791881088 && ker2_iang_sin <= 0.0739371534195933);
  __CPROVER_assume(ker2_from_index >= 1.0 && ker2_from_index <= 1.62759991014544);
  __CPROVER_assume(ker2_to_index >= 1.0 && ker2_to_index <= 1.62759991014544);
  __CPROVER_assume(ker2_slope_angle >= 0.0 && ker2_slope_angle <= 0.101615504753242);
  __CPROVER_assume(ker2_rad_curvature >= -78.0999999999999 && ker2_rad_curvature <= 27.05);
  numerical_kernel2(ker2_iang_sin, ker2_from_index, ker2_to_index,
    ker2_slope_angle, ker2_rad_curvature);
  return 0;
}
