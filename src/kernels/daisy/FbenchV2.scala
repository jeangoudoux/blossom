import daisy.lang._
import Real._


object Fbench_ver2 {

  def kernel1(iang_sin: Real, from_index: Real, to_index: Real,
    slope_angle: Real, obj_dist: Real, old_ray_height: Real): Real = {
    // BB + AFLGo

    require(-0.217475732986908 <= iang_sin && iang_sin <= 0.0739371534195933 && 1.0 <= from_index && from_index <= 1.62 &&
      1.0 <= to_index && to_index <= 1.62 && 0.0 <= slope_angle && slope_angle <= 0.0992 &&
      0.0 <= obj_dist && obj_dist <= 121.112061135518 && 1.97 <= old_ray_height && old_ray_height <= 2.0)

    val rang_sin = (from_index / to_index) * (iang_sin)
  
    val old_axis_slope_angle = slope_angle
  
    val axis_slope_angle = slope_angle + (iang_sin) - (rang_sin)
    val ray_height = if (obj_dist != 0.0) {   // this is essentially useless, since Daisy's domain cannot capture this
      obj_dist * (old_axis_slope_angle)
    } else {
      old_ray_height
    }
    val object_distance = ray_height / axis_slope_angle

    object_distance
  }

  
  def kernel2(iang_sin: Real, from_index: Real, to_index: Real,
    slope_angle: Real, rad_curvature: Real, radius_of_curvature: Real): Real = {  
    // BB ranges

    require(-0.219 <= iang_sin && iang_sin <= 0.0739371534195933 && 1.0 <= from_index && from_index <= 1.63 &&
      1.0 <= to_index && to_index <= 1.63 && 0.0 <= slope_angle && slope_angle <= 0.102 && 
      -78.0999999999999 <= rad_curvature && rad_curvature <= 27.05 &&
      -78.1 <= radius_of_curvature && radius_of_curvature <= 27.01)

    val iang = asin(iang_sin)
    val rang_sin = (from_index / to_index) * (iang_sin)
    val old_axis_slope_angle = slope_angle
    val axis_slope_angle = slope_angle + (iang) - asin(rang_sin)

    val sagitta = sin((old_axis_slope_angle + (iang)) / 2.0)
    val sagitta2 = 2.0 * radius_of_curvature * (sagitta) * (sagitta)
    
    val object_distance = ((rad_curvature * sin(old_axis_slope_angle + (iang))) *
              (1.0 / tan(axis_slope_angle))) + (sagitta2)
    object_distance
  }
}
