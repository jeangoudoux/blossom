import daisy.lang._
import Real._


object Nbody {

  def compute_velocity(velocity_x1: Real, velocity_y1: Real, velocity_z1: Real,
    velocity_x2: Real, velocity_y2: Real, velocity_z2: Real,
    velocity_x3: Real, velocity_y3: Real, velocity_z3: Real,
    accelerations_x1: Real, accelerations_y1: Real, accelerations_z1: Real,
    accelerations_x2: Real, accelerations_y2: Real, accelerations_z2: Real,
    accelerations_x3: Real, accelerations_y3: Real, 
    accelerations_z3: Real): (Real, Real, Real) = {
    // BB ranges
    require(-399.108805043489 <= velocity_x1 && velocity_x1 <= 140.395874122329 &&
      -197.053728533596 <= velocity_x2 && velocity_x2 <= 572.207879861596 &&
      -17145.0409716001 <= velocity_x3 && velocity_x3 <= 23554.068142828 &&
      -193.619502420673 <= velocity_y1 && velocity_y1 <= 269.94727755118 &&
      -452.770478818149 <= velocity_y2 && velocity_y2 <= 282.837886313683 &&
      -17481.5831794456 <= velocity_y3 && velocity_y3 <= 18458.1956527784 &&
      -394.13670682791 <= velocity_z1 && velocity_z1 <= 301.590181598338 &&
      -459.964415984446 <= velocity_z2 && velocity_z2 <= 605.252803824391 &&
      -18433.2758809848 <= velocity_z3 && velocity_z3 <= 19029.6301890438 &&
      -410.139852063405 <= accelerations_x1 && accelerations_x1 <= 412.074434438454 &&
      -773.374617254222 <= accelerations_x2 && accelerations_x2 <= 596.566959341908 &&
      -25257.1939468974 <= accelerations_x3 && accelerations_x3 <= 26122.2189362439 &&
      -209.228821604021 <= accelerations_y1 && accelerations_y1 <= 303.405888438039 &&
      -477.258175305645 <= accelerations_y2 && accelerations_y2 <= 291.977303179275 &&
      -49492.2053148291 <= accelerations_y3 && accelerations_y3 <= 18458.266842774 &&
      -397.851206364176 <= accelerations_z1 && accelerations_z1 <= 503.398256724025 &&
      -979.818814739621 <= accelerations_z2 && accelerations_z2 <= 632.265331759936 &&
      -18433.1523198009 <= accelerations_z3 && accelerations_z3 <=98210.1674057562)
      
    val velocities_x1 = velocity_x1 + accelerations_x1
    val velocities_x2 = velocity_x2 + accelerations_x2
    val velocities_x3 = velocity_x3 + accelerations_x3
    val velocities_y1 = velocity_y1 + accelerations_y1
    val velocities_y2 = velocity_y2 + accelerations_y2
    val velocities_y3 = velocity_y3 + accelerations_y3
    val velocities_z1 = velocity_z1 + accelerations_z1
    val velocities_z2 = velocity_z2 + accelerations_z2
    val velocities_z3 = velocity_z3 + accelerations_z3
    (velocities_x1, velocities_y1, velocities_z1) // only returning the first co-ordinate
  }


  def kernel(mass: Real, position_i_x: Real, position_i_y: Real, position_i_z: Real, 
    position_j_x: Real, position_j_y: Real, position_j_z: Real, 
    acceleration_x: Real, acceleration_y: Real, acceleration_z: Real): (Real, Real, Real) = {
    // BB ranges
    require(-86545.0148931074 <= position_i_x && position_i_x <= 150146.55756686 &&
      -106612.970838932 <= position_i_y && position_i_y <= 104328.238959767 && 
      -104129.64891511 <= position_i_z && position_i_z <= 121900.073601499 &&
      -86545.0148931074 <= position_j_x && position_j_x <= 150146.55756686 && 
      -106612.970838932 <= position_j_y && position_j_y <= 104328.238959767 &&
      -104129.64891511 <= position_j_z && position_j_z <= 121900.073601499 &&
      -25257.1955398914 <= acceleration_x && acceleration_x <= 26122.2164987853 && 
      -49492.2057046081 <= acceleration_y && acceleration_y <= 18458.2662415526 &&
      -18433.1517005512 <= acceleration_z && acceleration_z <= 98210.1684896515 && 
      0.9 <= mass && mass <= 1.1)
      
      val GravConstant: Real = 0.01
  
      val a_x = position_i_x - position_j_x
      val a_y = position_i_y - position_j_y
      val a_z = position_i_z - position_j_z

      val mod = sqrt(a_x*a_x + a_y*a_y + a_z*a_z)   // this is most likely going to complain about sqrt of neg. number

      val scale = GravConstant*mass/(mod * mod * mod)

      val new_accel_x = (acceleration_x + (scale * (position_j_x - position_i_x)))
      val new_accel_y = (acceleration_y + (scale * (position_j_y - position_i_y)))
      val new_accel_z = (acceleration_z + (scale * (position_j_z - position_i_z)))

      (new_accel_x, new_accel_y, new_accel_z)
  }  

}