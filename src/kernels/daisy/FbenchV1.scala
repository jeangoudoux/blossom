import daisy.lang._
import Real._


object FbenchV1 {
// BB + AFLGo ranges
  def kernel1(x: Real): (Real, Real) = {
    require(0.0412 <= x && x <= 0.225)

    val z = x * x
    val b = ((((893025.0 * z + 49116375.0) * z + 425675250.0) * z +
            1277025750.0) * z + 1550674125.0) * z + 654729075.0
    val a = (((13852575.0 * z + 216602100.0) * z + 891080190.0) * z +
            1332431100.0) * z + 654729075.0
    (a, b)
  }

  def kernel2(iang_sin: Real, from_index: Real, to_index: Real,
    slope_angle: Real, obj_dist: Real, old_ray_height: Real): Real = {
    // BB ranges
    require(-0.217475732986908 <= iang_sin && iang_sin <= 0.0739371534195933 && 1.0 <= from_index && from_index <= 1.6164 &&
      1.0 <= to_index && to_index <= 1.6164 && 0.0 <= slope_angle && slope_angle <= 0.0991744034677826 &&
      0.0 <= obj_dist && obj_dist <= 121.112061135518 && 0.0 <= old_ray_height && old_ray_height <= 2.0)

    val rang_sin = (from_index / to_index) * (iang_sin)
  
    val old_axis_slope_angle = slope_angle
  
    val axis_slope_angle = slope_angle + (iang_sin) - (rang_sin)
    val ray_height = if (obj_dist != 0.0) {   // this is essentially useless, since Daisy's domain cannot capture this
      obj_dist * (old_axis_slope_angle)
    } else {
      old_ray_height
    }
    val object_distance = ray_height / axis_slope_angle

    object_distance
  }

  
  def kernel3(iang_sin: Real, from_index: Real, to_index: Real,
    slope_angle: Real, rad_curvature: Real, radius_of_curvature: Real): Real = {  
    // BB + AFLGo rangess

    require(-0.219 <= iang_sin && iang_sin <= 0.0739371534195933 && 1.0 <= from_index && from_index <= 1.63 &&
      1.0 <= to_index && to_index <= 1.63 && 0.0 <= slope_angle && slope_angle <= 0.102 && 
      -78.0999999999999 <= rad_curvature && rad_curvature <= 27.05 &&
      -78.1 <= radius_of_curvature && radius_of_curvature <= 27.05)

    val iang = asin(iang_sin)
    val rang_sin = (from_index / to_index) * (iang_sin)
    val old_axis_slope_angle = slope_angle
    val axis_slope_angle = slope_angle + (iang) - asin(rang_sin)

    val sagitta = sin((old_axis_slope_angle + (iang)) / 2.0)
    val sagitta2 = 2.0 * radius_of_curvature * (sagitta) * (sagitta)
    
    val object_distance = ((rad_curvature * sin(old_axis_slope_angle + (iang))) *
              (1.0 / tan(axis_slope_angle))) + (sagitta2)
    object_distance
  }

  def kernel5(x: Real): Real = {
    // BB ranges
    require(0.0126130928245183 <= x && x <= 1.55487114228014)

    val fouroverpi: Real = 4.0 / 3.1415926535897932
    val piover4: Real = 3.1415926535897932 / 4.0
    val piover2: Real = 3.1415926535897932 / 2.0

    if (x < piover4) {
      val y = x * fouroverpi
      val z = y * y
      y * (((((((-0.202253129293E-13 * z + 0.69481520350522E-11) * z -
          0.17572474176170806E-8) * z + 0.313361688917325348E-6) * z -
          0.365762041821464001E-4) * z + 0.249039457019271628E-2) * z -
          0.0807455121882807815) * z + 0.785398163397448310)
    } else {
      val y = (piover2 - x) * fouroverpi
      val z = y * y
      ((((((-0.38577620372E-12 * z + 0.11500497024263E-9) * z -
        0.2461136382637005E-7) * z + 0.359086044588581953E-5) * z -
        0.325991886926687550E-3) * z + 0.0158543442438154109) * z -
        0.308425137534042452) * z + 1.0
    }
  }
  
}
