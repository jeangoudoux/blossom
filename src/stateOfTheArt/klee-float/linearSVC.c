#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include "klee/klee.h"

double coefficients[3][4] = {{0.1842392154564795, 0.451227272556133, -0.8079415307840663, -0.45071538868087374}, {0.05674216334215214, -0.8981194157558171, 0.4083958984619029, -0.9606849222530257}, {-0.8507702027651778, -0.9868272809923471, 1.3808941178761502, 1.8652996296254423}};
double intercepts[3] = {0.10956209204126635, 1.677892675644221, -1.7095873485238913};

double numerical_kernel(double features[], int i){
  double prob = 0.0;
  for (int j = 0, jl = sizeof(coefficients[0]) / sizeof (coefficients[0][0]); j < jl; j++) {
    prob += coefficients[i][j] * features[j];
  }
  assert(!isinf(prob));
  assert(!isnan(prob));
  return prob;
}

int predict (double features[]) {
  double class_val = -INFINITY;
  int class_idx = -1;
  int i, il, j, jl;
  for (i = 0, il = sizeof(coefficients) / sizeof (coefficients[0]); i < il; i++) {
    double prob = numerical_kernel(features, i);
    if (prob + intercepts[i] > class_val) {
      class_val = prob + intercepts[i];
      class_idx = i;
    }
  }
  return class_idx;
}

int main(void) {
  double features[4];
  klee_make_symbolic(features, sizeof(double)*4, "features");

  for(int j = 0; j < 1000;j++) {
    klee_prefer_cex(features, 4.0 <= features[0] && features[0] <= 8.0);
    klee_prefer_cex(features, 2.0 <= features[1] && features[1] <= 4.5);
    klee_prefer_cex(features, 1.0 <= features[2] && features[2] <= 7.0);
    klee_prefer_cex(features, 0.1 <= features[3] && features[3] <= 2.5);
    predict(features);
  }
  return 0;
}
