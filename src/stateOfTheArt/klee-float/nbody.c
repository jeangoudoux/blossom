#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <float.h>
#include "klee/klee.h"

typedef struct vector {
  double x,y,z;
}vector;

#define bodies 3

double GravConstant = 0.01;
int timeSteps = 10;

vector addVectors(vector a,vector b){
  vector c = {a.x+b.x,a.y+b.y,a.z+b.z};

  return c;
}

vector scaleVector(double b,vector a){
  vector c = {b*a.x,b*a.y,b*a.z};

  return c;
}

vector subtractVectors(vector a,vector b){
  vector c = {a.x-b.x,a.y-b.y,a.z-b.z};

  return c;
}

double mod(vector a){
  return sqrt(a.x*a.x + a.y*a.y + a.z*a.z);
}

void resolveCollisions(vector * positions, vector * velocities){
  int i,j;
  for(i=0;i<bodies-1;i++)
    for(j=i+1;j<bodies;j++){
      if(positions[i].x==positions[j].x && positions[i].y==positions[j].y && positions[i].z==positions[j].z){
        vector temp = velocities[i];
        velocities[i] = velocities[j];
        velocities[j] = temp;
      }
    }
}

vector numerical_kernel1(double mass, vector position_i, vector position_j, vector acceleration) {
  vector acceleration_computed = addVectors(acceleration,scaleVector(GravConstant*mass/
  pow(mod(subtractVectors(position_i,position_j)),3),subtractVectors(position_j,position_i)));
  assert(!isinf(acceleration_computed.x));
  assert(!isnan(acceleration_computed.x));
  assert(!isinf(acceleration_computed.y));
  assert(!isnan(acceleration_computed.y));
  assert(!isinf(acceleration_computed.z));
  assert(!isnan(acceleration_computed.z));
  return acceleration_computed;
}

void computeAccelerations(double masses[], vector * positions, vector * accelerations){
  int i,j;
  for(i=0;i<bodies;i++){
    accelerations[i].x = 0;
    accelerations[i].y = 0;
    accelerations[i].z = 0;
    for(j=0;j<bodies;j++){
      if(i!=j){
        accelerations[i] = numerical_kernel1(masses[j], positions[i], positions[j], accelerations[i]);
      }
    }
  }
}

void computePositions(vector *positions, vector *velocities, vector *accelerations){
  int i;
  for(i=0;i<bodies;i++)
    positions[i] = addVectors(positions[i],addVectors(velocities[i],scaleVector(0.5,accelerations[i])));
}

void numerical_kernel2(vector *velocities, vector *accelerations){
  int i;

  for(i=0;i<bodies;i++) {
    velocities[i] = addVectors(velocities[i],accelerations[i]);
    assert(!isinf(velocities[i].x));
    assert(!isnan(velocities[i].x));
    assert(!isinf(velocities[i].y));
    assert(!isnan(velocities[i].y));
    assert(!isinf(velocities[i].z));
    assert(!isnan(velocities[i].z));
  }
}

void simulate(double masses[], vector *positions, vector *velocities, vector *accelerations){
  computeAccelerations(masses, positions, accelerations);
  computePositions(positions, velocities, accelerations);
  numerical_kernel2(velocities, accelerations);
  resolveCollisions(positions, velocities);
}

int main(void) {
  int i;
  double masses[3];
  vector positions[3];
  vector velocities[3];
  vector accelerations[3];
  klee_make_symbolic(masses, sizeof(double)*3, "masses");
  klee_make_symbolic(positions, sizeof(double)*9, "positions");
  klee_make_symbolic(velocities, sizeof(double)*9, "velocities");

  klee_prefer_cex(masses[0], 0.9 <= masses[0] && masses[0] <= 1.1);
  klee_prefer_cex(masses[1], 0.001 <= masses[1] && masses[1] <= 0.9);
  klee_prefer_cex(masses[2], 0.0001 <= masses[2] && masses[2] <= 0.0019);

  klee_prefer_cex(positions[0].x, 0.0 <= positions[0].x && positions[0].x <= 0.5);
  klee_prefer_cex(positions[0].y, 0.0 <= positions[0].y && positions[0].y <= 0.5);
  klee_prefer_cex(positions[0].z, 0.0 <= positions[0].z && positions[0].z <= 0.5);  
  klee_prefer_cex(positions[1].x, 0.9 <= positions[1].x && positions[1].x <= 1.1);
  klee_prefer_cex(positions[1].y, 0.9 <= positions[1].y && positions[1].y <= 1.1);
  klee_prefer_cex(positions[1].z, 0.0 <= positions[1].z && positions[1].z <= 0.5);
  klee_prefer_cex(positions[2].x, 0.0 <= positions[2].x && positions[2].x <= 0.5);
  klee_prefer_cex(positions[2].y, 0.9 <= positions[2].y && positions[2].y <= 1.1);
  klee_prefer_cex(positions[2].z, 0.9 <= positions[2].z && positions[2].z <= 1.1);

  klee_prefer_cex(velocities[0].x, 0.001 <= velocities[0].x && velocities[0].x <= 0.019);
  klee_prefer_cex(velocities[0].y, 0.0 <= velocities[0].y && velocities[0].y <= 0.5);
  klee_prefer_cex(velocities[0].z, 0.0 <= velocities[0].z && velocities[0].z <= 0.5);
  klee_prefer_cex(velocities[1].x, 0.0 <= velocities[1].x && velocities[1].x <= 0.5);
  klee_prefer_cex(velocities[1].y, 0.0 <= velocities[1].y && velocities[1].y <= 0.5);
  klee_prefer_cex(velocities[1].z, 0.01 <= velocities[1].z && velocities[1].z <= 0.03);
  klee_prefer_cex(velocities[2].x, 0.001 <= velocities[2].x && velocities[2].x <= 0.019);
  klee_prefer_cex(velocities[2].y, -0.019 <= velocities[2].y && velocities[2].y <= -0.001);
  klee_prefer_cex(velocities[2].z, -0.019 <= velocities[2].z && velocities[2].z <= -0.001);

  for(i=0;i<timeSteps;i++){
    simulate(masses, positions, velocities, accelerations);
  }
  return 0;
}

