#! /bin/bash
# AFLGO installation script with its dependencies
# inspired by https://github.com/aflgo/aflgo/blob/master/scripts/build/aflgo-build.sh
# updated version for LLVM 4.0.1

# build clang & LLVM
LLVM_DEP_PACKAGES="build-essential make cmake ninja-build git subversion python2.7 binutils-gold binutils-dev curl wget"
sudo apt-get install -y $LLVM_DEP_PACKAGES 

# LLVM 4.0.1
mkdir ~/build; cd ~/build
mkdir llvm_tools; cd llvm_tools
wget http://releases.llvm.org/4.0.1/llvm-4.0.1.src.tar.xz
wget http://releases.llvm.org/4.0.1/cfe-4.0.1.src.tar.xz
wget http://releases.llvm.org/4.0.1/compiler-rt-4.0.1.src.tar.xz
wget http://releases.llvm.org/4.0.1/libcxx-4.0.1.src.tar.xz
wget http://releases.llvm.org/4.0.1/libcxxabi-4.0.1.src.tar.xz
tar xf llvm-4.0.1.src.tar.xz
tar xf cfe-4.0.1.src.tar.xz
tar xf compiler-rt-4.0.1.src.tar.xz
tar xf libcxx-4.0.1.src.tar.xz
tar xf libcxxabi-4.0.1.src.tar.xz
mv cfe-4.0.1.src ~/build/llvm_tools/llvm-4.0.1.src/tools/clang
mv compiler-rt-4.0.1.src ~/build/llvm_tools/llvm-4.0.1.src/projects/compiler-rt
mv libcxx-4.0.1.src ~/build/llvm_tools/llvm-4.0.1.src/projects/libcxx
mv libcxxabi-4.0.1.src ~/build/llvm_tools/llvm-4.0.1.src/projects/libcxxabi
mkdir -p build-llvm/llvm; cd build-llvm/llvm
cmake -G "Ninja" \
      -DLIBCXX_ENABLE_SHARED=OFF -DLIBCXX_ENABLE_STATIC_ABI_LIBRARY=ON \
      -DCMAKE_BUILD_TYPE=Release -DLLVM_TARGETS_TO_BUILD="X86" \
      -DLLVM_BINUTILS_INCDIR=/usr/include ~/build/llvm_tools/llvm-4.0.1.src
ninja; sudo ninja install

cd ~/build/llvm_tools
mkdir -p build-llvm/msan; cd build-llvm/msan
cmake -G "Ninja" \
      -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ \
      -DLLVM_USE_SANITIZER=Memory -DCMAKE_INSTALL_PREFIX=/usr/msan/ \
      -DLIBCXX_ENABLE_SHARED=OFF -DLIBCXX_ENABLE_STATIC_ABI_LIBRARY=ON \
      -DCMAKE_BUILD_TYPE=Release -DLLVM_TARGETS_TO_BUILD="X86" \
       ~/build/llvm_tools/llvm-4.0.1.src
ninja cxx; sudo ninja install-cxx

# install LLVMgold in bfd-plugins
sudo mkdir /usr/lib/bfd-plugins
sudo cp /usr/local/lib/libLTO.so /usr/lib/bfd-plugins
sudo cp /usr/local/lib/LLVMgold.so /usr/lib/bfd-plugins

# install some packages
export LC_ALL=C
sudo apt-get update
sudo apt install -y python-dev python3 python3-dev python3-pip autoconf automake libtool-bin python-bs4 libclang-4.0-dev 
# fix bugs
sudo pip3 install --upgrade pip
sudo pip install xlsxwriter pycrypto networkx pydot pydotplus
sudo apt-get --reinstall install libc6 libc6-dev
sudo apt-get install gcc-multilib g++-multilib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib

# TODO
# execute as a super user (execute at the terminal creation?)
# echo core >/proc/sys/kernel/core_pattern

# build AFLGo
cd $HOME; git clone https://github.com/aflgo/aflgo.git
cd aflgo; make clean all; cd llvm_mode; make clean all
export AFLGO=$HOME/aflgo
